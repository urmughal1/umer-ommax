# Ommax Coding Challenge - Data Engineering
## Python - Algorithmic

You were given two data sets containing address data. The first dataset is from the ERP system of a client. The second dataset includes data from potential prospects offered by an address broker.

Implement functions to compare the two datasets and flag entries the customer already has in the ERP system. Document and present your approach.

You can find the dataset here: [Click](https://ada-application-challenge.s3.eu-central-1.amazonaws.com/python_algorithm_dataset.zip)