# Ommax Coding Challenge - Data Engineering
## Airflow

One of your coworkers started to write a DAG but is now staffed on another project. You are given the task to complete the DAG. The purpose of the DAG is to query data from an API for multiple clients. The client data might include spending information in different currencies which should be converted. The data should be queried and saved to a S3 bucket. Afterwards, it is transferred to a staging database schema where conversions and cleanups should be done. In the end the final data is transferred to the production schema. Only data which is not already in the database should be queried, but the API documentation tells you that data within the last 30 days is unstable and can change.

```python
from datetime import datetime
from airflow import DAG
from airflow.models import Variable
from airflow.operators.dummy import DummyOperator

DAG_DOC_MD = """Lorem Ipsum"""

ARGS = {
    "owner": "Ommax",
    "start_date": datetime(2021, 1, 1),
    "retries": 0,
}

S3_CONN_ID = "sample_s3"
S3_BUCKET = "sample_bucket"
REDSHIFT_CONNECTOR_ID = "sample_redshift"
REDSHIFT_SCHEMA = "sample_schema"
REDSHIFT_SCHEMA_STAGING = "sample_schema_staging"

API_ACCESS_CREDENTIALS = Variable.get("sample_credentials", deserialize_json=True)

with DAG(
    dag_id="QueryDataFromAPI",
    default_args=ARGS,
    schedule_interval=None,
    catchup=False,
    doc_md=DAG_DOC_MD,
) as dag:
    query_account_ids = DummyOperator(
        task_id="query_client_account_ids",
        credentials=API_ACCESS_CREDENTIALS,
    )
```

Please only use dummy operators (assuming they have real functionality) to solve the task. Don't forget to pass the right parameters to the operators and add documentation to each one explaining in detail the execution steps and logic. Think about different DAG structures you could use for this task and pick the most efficient one. Explain and justify your design decision.