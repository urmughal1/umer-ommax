# Ommax Coding Challenge - Data Engineering
## SQL

There are two tables in your PostgreSQL database - `Employees` and `Salaries`:

### Employees
```sql
INSERT INTO Employees (id, first_name, last_name, company_level, bonus_achievement)
VALUES (1, 'Harald', 'Groninger', 2, 0.45),
       (2, 'Peter', 'Underberg', 5, 1.2),
       (3, 'Joe', 'Donut', 4, 1.1);
```

id|first_name|last_name|company_level|bonus_achievement
---|---|---|---|---
1|"Harald"|"Groninger"|2|0.45
2|"Peter"|"Underberg"|5|1.2
3|"Joe"|"Donut"|4|1.1

### Salaries
```sql
INSERT INTO Salaries (company_level, base_salary, bonus_salary)
VALUES (1, 40000, 2500),
       (2, 50000, 5000),
       (3, 65000, 7500),
       (4, 80000, 12500),
       (5, 100000, 20000);
```

company_level|base_salary|bonus_salary
---|---|---
1|40000|2500
2|50000|5000
3|65000|7500
4|80000|12500
5|100000|20000

### Task

Combine the two tables into one table with the following target format:

full_name|base_salary|bonus_salary|total_salary
---|---|---|---
"Peter Underberg"|100000|24000|124000
"Joe Donut"|80000|13750|93750
"Harald Groninger"|50000|0|50000

Note: The bonus_salary amount is the amount an employee gets when he reaches 100% goal achievement. If he reaches 200% he gets paid double the amount. He only gets a bonus payment if he reaches at least 50% goal achievement.

Provide the SQL statements needed for this transformation. You can spin up a local database, get a free plan from https://elements.heroku.com/addons/heroku-postgresql or https://www.elephantsql.com or use https://extendsclass.com/postgresql-online.html.
