# Umer_mughal Coding Challenge - Data Engineering
## Python - Async Report

You are given the task to query async reports from an API. Implement the `handle_async_report_run` function without changing the `AsyncReportGenerator` class. Think of additional edge cases that could occur with an API and try to handle those aswell.

```python
import random
from typing import Dict


class AsyncReportGenerator:
    states = {0: "Initiating", 1: "Running", 2: "Failed", 3: "Unknown", 4: "Finished"}

    def __init__(self, rnd_seed: int = 0):
        random.seed(rnd_seed)
        self.rnd_seed = rnd_seed

        self.status = AsyncReportGenerator.states[0]
        self.progress = None
        self.query_size = random.randint(1, 10000)
        self.halted = False

    def __get_status_helper(self) -> Dict:
        return {"status": self.status, "progress": self.progress}

    def get_status(self) -> Dict:
        if random.randint(1, 60) == 1:
            self.halted = True
        if self.halted:
            return self.__get_status_helper()
        if random.randint(1, 60) == 1:
            self.status = AsyncReportGenerator.states[2]

        if self.status == AsyncReportGenerator.states[0]:
            if random.randint(1, 5) == 1:
                self.status = AsyncReportGenerator.states[1]
                self.progress = 0
        elif self.status == AsyncReportGenerator.states[1]:
            if self.progress == 100 and random.randint(1, 2) == 1:
                self.status = AsyncReportGenerator.states[4]
            query_chunk = random.randint(1, self.query_size*10)
            progress = int(query_chunk / self.query_size)
            if progress + self.progress > 100:
                self.progress = 100
            else:
                self.progress += progress
        elif self.status == AsyncReportGenerator.states[4]:
            self.status = AsyncReportGenerator.states[3]

        return self.__get_status_helper()

    def get_data(self) -> str:
        if self.status == AsyncReportGenerator.states[4]:
            return "Important insights"
        else:
            self.status = AsyncReportGenerator.states[2]


def handle_async_report_run(seed: int = 0):
    # ToDo
    report = AsyncReportGenerator(rnd_seed=seed)
```