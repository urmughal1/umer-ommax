/*
creating the tables into existing database
*/
CREATE TABLE Employees (
    id int CONSTRAINT firstkey PRIMARY KEY ,
    first_name varchar(50),
    last_name varchar(50),
    company_level int ,
    bonus_achievement float
);
CREATE TABLE Salaries (
    company_level int CONSTRAINT secondkey PRIMARY KEY ,
    base_salary bigint ,
    bonus_salary bigint
);

/*
inserting into the tables
*/
INSERT INTO Employees (id, first_name, last_name, company_level, bonus_achievement)
VALUES (1, 'Harald', 'Groninger', 2, 0.45),
       (2, 'Peter', 'Underberg', 5, 1.2),
       (3, 'Joe', 'Donut', 4, 1.1);


INSERT INTO Salaries (company_level, base_salary, bonus_salary)
VALUES (1, 40000, 2500),
       (2, 50000, 5000),
       (3, 65000, 7500),
       (4, 80000, 12500),
       (5, 100000, 20000);

/*
this query is responsible for calculating the bonus salary
and this function is working with almost every database
we can use procedure and/or function to implement it
*/
SELECT concat('"',e.first_name,' ', e.last_name,'"') as full_name,
       s.base_salary as base_salary,
       CASE WHEN (e.bonus_achievement > 0.50)
           THEN FLOOR(((s.bonus_salary/100) *(e.bonus_achievement * 100)))
           ELSE 0
           END AS bonus_salaray,
       CASE WHEN (e.bonus_achievement > 0.50)
           THEN FLOOR(s.base_salary+((s.bonus_salary/100) *(e.bonus_achievement * 100)))
           ELSE s.base_salary
           END AS total_salary
from  Employees as e
JOIN Salaries as s on  e.company_level = s.company_level
;


/*
this query is responsible for calculating the bonus salary
and this function is tested on MySql 5.3 and earlier version and
its working but not working on Postgresql
*/
SELECT concat('"',e.first_name,' ', e.last_name,'"') as full_name,
       s.base_salary as base_salary,
       IF((e.bonus_achievement > 0.50),
           FLOOR(((s.bonus_salary / 100) * (e.bonus_achievement * 100))),
           0) AS bonus_salaray,
       IF((e.bonus_achievement > 0.50),
           FLOOR(s.base_salary + ((s.bonus_salary / 100) * (e.bonus_achievement * 100))),
          s.base_salary) AS total_salary
from  Employees as e
JOIN Salaries as s on  e.company_level=s.company_level
;