"""
@Author: Umer Mughal
@Email: urmughal1@gmail.com


Here are some testcases that i have implemented
"""
from unittest import TestCase
from AddressMatchingAlgorithm.AddressComparisonAlgo import AddressComparisonAlgo
from AddressMatchingAlgorithm.FileHandler import FileHandler


class FooTest(TestCase):

    def test_set_algo(self):
        addressCompAlgo = AddressComparisonAlgo()
        res = addressCompAlgo.address_match_using_set('Apple Inc', 'apple Inc')
        self.assertGreater(res, 80)

    def test_leven_algo(self):
        addressCompAlgo = AddressComparisonAlgo()
        res = addressCompAlgo.levenshtein_address_match('Apple Inc', 'apple Inc')
        self.assertGreater(res[1], 80)

    def test_fuzzy_algo(self):
        addressCompAlgo = AddressComparisonAlgo()
        res = addressCompAlgo.fuzzy_wuzzy_address_match('Apple Inc', 'apple Inc')
        self.assertGreater(res[2], 80)

    def test_file(self):
        file = FileHandler('./../src/python_algorithm_dataset/test.csv')
        data = file.file_reading_by_dataframe()
        self.assertGreater(len(data), 0)
