"""
@Author: Umer Mughal
@Email: urmughal1@gmail.com

Note: previously with 0(zero) knowledge for creating the airflow file.
i tried my best and given the fact that information provided is not enough to test.
with possible assumption file is created.

But the plus point is now i have some idea how its working.
"""
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from datetime import datetime
from airflow.models import Variable
import psycopg2
import requests
import logging
import json
import boto3

DAG_DOC_MD = """Lorem Ipsum"""

S3_CONN_ID = "sample_s3"
S3_BUCKET = "sample_bucket"
S3_ACCESS_KEY = "access_key_to_s3"
S3_REGION_NAME = "region_name"

REDSHIFT_CONNECTOR_ID = "sample_redshift"
REDSHIFT_SCHEMA = "sample_schema"
REDSHIFT_SCHEMA_STAGING = "sample_schema_staging"

API_ACCESS_CREDENTIALS = Variable.get("sample_credentials", deserialize_json=True)


def fetch_api_data(**context):
    """
    Gets api data from client

    conditions:
    1. get data at first call (its not supporting the pagination yet)
    2. for multiple resources we need to iterate it
    3. with possibly getting the data in base currency

    """
    try:
        logging.info('Data Extraction Start from here')
        task_instance = context['ti']
        response = requests.get(API_ACCESS_CREDENTIALS['url'])

        if response.status_code == 200:
            result = json.loads(response.text)
            task_instance.xcom_push(key='client_data', value=result)

        raise Exception('data request failed with error code {}'.format(response.status_code))
    except Exception as e:
        print("Data Extraction: Failed")
        print(e)

def saving_data_to_s3(**context):
    """
    boto3 library is used to save the data into aws s3
    :param context:
    :return:
    """

    try:
        client_data = context.get('ti').xcom_pull(key='client_data')

        s3 = boto3.resource(
            's3',
            region_name='us-east-1',
            aws_access_key_id=S3_CONN_ID,
            aws_secret_access_key=S3_ACCESS_KEY
        )
        s3.Object(S3_BUCKET,
                  API_ACCESS_CREDENTIALS['url'] + '/' +
                  datetime.now().strftime("%d/%m/%Y") + '/' +
                  datetime.now().strftime("%d/%m/%Y") + '.txt').put(Body=client_data)

    except Exception as e:
        print('S3: oh there is problem in s3 connection or saving the data')
        print(e)


def staging_server_convert_cleanup(**context):
    """
    there are couple of things are happening here
    1. getting the client data
    2. processing the client data to different currency
    3. making connection to staging database
    4. updating the data if exist or creating new one
    5. committing to database

    NOTE: this process can be divided into multiple modules

    :param context:
    :return:
    """

    try:
        logging.info('currency conversion with other operations and storing into staging')
        client_data = context.get('ti').xcom_pull(key='client_data')
        # process the currency data
        currency_processed_data = client_data
        connection = psycopg2.connect(
            "dbname=" + REDSHIFT_SCHEMA_STAGING + " host=" + REDSHIFT_SCHEMA + " port=5439 user=master "
                                                                               "password=" + REDSHIFT_CONNECTOR_ID)
        upsert_qry = "creating the query and adding the data ->" + currency_processed_data
        c = connection.cursor()
        c.execute(upsert_qry)
        connection.commit()

    except Exception as e:
        print('REDSHIFT: problem in connecting the red shift or sending the data ')
        print(e)


def saving_data_to_production():
    """
    i am assuming here to synchronized the data
    we have get from the staging and updating or
    inserting into production
    :return:
    """
    try:
        logging.info('saving to production database')

        staging_connection = psycopg2.connect(
            "dbname=" + REDSHIFT_SCHEMA_STAGING + " host=" + REDSHIFT_SCHEMA + " port=5439 user=master "
                                                                               "password=" + REDSHIFT_CONNECTOR_ID)
        get_staging_data_qry = "getting the staging data"
        staging_cur = staging_connection.cursor()
        staging_cur.execute(get_staging_data_qry)
        staging_data = staging_cur.fetchall()

        production_connection = psycopg2.connect("PRODUCTION")
        upsert_qry = "creating the query and adding the data or updating it ->" + staging_data
        production_cur = production_connection.cursor()
        production_cur.execute(upsert_qry)
        production_connection.commit()

        staging_cur.close()
        staging_connection.close()

        production_cur.close()
        production_connection.close()

    except Exception as e:
        print('REDSHIFT: problem in connecting the red shift or sending the data ')
        print(e)


"""
Default arguments 
"""
ARGS = {
    "owner": "Ommax",
    "start_date": datetime(2021, 1, 1),
    'retries': 0,
}

"""
This is the dag starting point
"""
dag = DAG(
    dag_id='QueryDataFromAPI',
    default_args=ARGS,
    schedule_interval=None,
    catchup=False,
    doc_md=DAG_DOC_MD,
)

"""
fetching the data from different client(s)

"""
fetch_api_data = DummyOperator(
    task_id='fetch_api_data',
    python_callable=fetch_api_data,
    provided_context=True,
    dag=dag)

"""
saving the data to s3
"""
saving_data_to_s3 = DummyOperator(
    task_id='saving_data_to_s3',
    python_callable=saving_data_to_s3,
    provided_context=True,
    dag=dag)

"""
cleaning converting and storing to staging REDSHIFT
"""
staging_server_convert_cleanup = DummyOperator(
    task_id='staging_server_convert_cleanup',
    python_callable=staging_server_convert_cleanup,
    provided_context=True,
    dag=dag)

"""
getting from staging and saving to production
"""
saving_data_to_production = DummyOperator(
    task_id='saving_data_to_production',
    python_callable=saving_data_to_production,
    dag=dag)

fetch_api_data >> saving_data_to_s3 >> staging_server_convert_cleanup >> saving_data_to_production