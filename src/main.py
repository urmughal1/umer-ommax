"""
@Author: Umer Mughal
@Email: urmughal1@gmail.com

Please follow the README.md for more detail
"""
from datetime import timedelta, datetime
from AddressMatchingAlgorithm.FileHandler import FileHandler
from AddressMatchingAlgorithm.AddressComparisonAlgo import AddressComparisonAlgo
from AsyncReportGenerator.AsyncReportGenerator import AsyncReportGenerator
import asyncio


async def handle_async_report_run(seed: int = 0):
    """
    this function will handle the Async Report Generator
    and will responsible for calling the current_report_status function
    :param seed:
    :return:
    """
    report = AsyncReportGenerator(rnd_seed=seed)
    task = asyncio.create_task(current_report_status(report))
    return await task


async def current_report_status(report, time_threshold_second=4, waiting_time=0.5):
    """
    there are some control variables to handle the edge cases
    :param report:
    :param time_threshold_second:
    :param waiting_time:
    :return:
    """
    try:
        end_date = datetime.now() + timedelta(seconds=time_threshold_second)
        if report.get_status().get("status") == 'Failed':
            raise Exception("AsyncReportGenerator: Report Generation failed")

        while report.get_status().get("progress") < 100:
            await asyncio.sleep(waiting_time)
            print(report.get_status())
            print(report.get_data())

            if report.get_status().get("status") == 'Failed':
                raise Exception("AsyncReportGenerator: Report Generation failed")

            if datetime.now() >= end_date and report.get_status().get("progress") <= 100:
                raise Exception("AsyncReportGenerator: Report Generation time exceed the time limit")

    except Exception as e:
        print("AsyncReportGenerator: Report Generation has some unknown error")
        print(e)

    return report.get_status(), report.get_data()

if __name__ == '__main__':
    """
    TASK3-:Async report generator test
    """
    print(asyncio.run(handle_async_report_run()))

    """
    Reading the file
    """
    customer_erp_file = FileHandler('./../src/python_algorithm_dataset/customer_erp_data.csv')
    broker_dataset_file = FileHandler('./../src/python_algorithm_dataset/data_broker_dataset.csv')

    """
    filling some nan value
    """
    customer_erp_data = customer_erp_file.file_reading_by_dataframe().fillna('')
    broker_dataset = broker_dataset_file.file_reading_by_dataframe().fillna('')

    """
    address data including only postcode
    """
    customer_erp_data_with_postcode = customer_erp_data['address'] + ' ' + customer_erp_data['postcode'].astype(str)
    broker_dataset_with_postcode = broker_dataset['address'] + ' ' + broker_dataset['postcode'].astype(str)

    """
    address data including postcode and number 
    by assuming that number is part of address like house number
    """
    customer_erp_data_with_postcode_and_number = customer_erp_data_with_postcode + ' ' + customer_erp_data[
        'number'].astype(str)

    """
    processing two address datasets
    """
    address_with_postcode_comp_algo = AddressComparisonAlgo(broker_dataset_with_postcode,
                                                            customer_erp_data_with_postcode)
    address_with_postcode_and_number_comp_algo = AddressComparisonAlgo(broker_dataset_with_postcode,
                                                                       customer_erp_data_with_postcode_and_number)

    """
    putting the flag into csv files
    filename
    1. address_with_postcode_comp.csv
    2. address_with_postcode_and_number_comp.csv
    3. address_with_postcode_comp_with_fuzz.csv
    4. address_with_postcode_and_number_comp_with_fuzz.csv
    you can find those files in the 
    ROOT_DIR/src/python_algorithm_dataset/
    # """
    broker_dataset["flag"] = address_with_postcode_comp_algo.address_match_process()
    # broker_dataset.to_csv('./../src/python_algorithm_dataset/address_with_postcode_comp_with_fuzzy.csv', index=False)
    #
    # broker_dataset.drop(['flag'], axis=1)
    #
    broker_dataset["flag"] = address_with_postcode_and_number_comp_algo.address_match_process()
    # broker_dataset.to_csv('./../src/python_algorithm_dataset/address_with_postcode_and_number_comp_with_fuzzy.csv', index=False)
