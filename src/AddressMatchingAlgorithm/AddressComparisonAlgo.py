"""
@Author: Umer Mughal
@Email: urmughal1@gmail.com

there are many algorithms but i have chosen some.
the purpose of this code to find the proper address from erp of client

TODO:if i have more time i would like to test with pyspark and.
"""
import Levenshtein as lev
from fuzzywuzzy import fuzz
import pandas as pd


class AddressComparisonAlgo:
    def __init__(self, address_data1='', address_data2=''):
        self.address_data1 = address_data1
        self.address_data2 = address_data2

    def levenshtein_address_match(self, address1, address2):
        """
        Most commonly used function and its pretty straight forward.
        it calculates the distance between two strings a and b and
        provide the ratio and distance in output

        pros: fast solution, widely used, efficient with same number of words in two strings
        cons: not good with different order of words in two string, spelled out of order,considerable spelling variatio

        Results:
        1. 107 matches without number
        2. 81 matches with number

        :param address1:
        :param address2:
        :return:
        """
        return [lev.distance(address1.lower(), address2.lower()), lev.ratio(address1.lower(), address2.lower()) * 100]

    def fuzzy_wuzzy_address_match(self, address1, address2):
        """
        good algorithm to process the matching of two string

        pros:good with different order of words in two string, spelled out of order,considerable
        cons:not fast since it has to do sorting, comparing and calculating the accuracy

        Results:
        1. 953 matches without number column included
        2. 748 matches with number column included

        :param address1:
        :param address2:
        :return:
        """
        address1 = address1.lower()
        address2 = address2.lower()
        return [fuzz.ratio(address1, address2),
                fuzz.partial_ratio(address1, address2),
                fuzz.token_set_ratio(address1, address2)]

    def address_match_using_set(self, address1, address2):
        """
        this is the fast algorithm which is based on set which provide the
        fast solution as set has deletion, insertion big-O(1)
        pros: fast
        cons: work with same no of words in two strings

        Results:
        1. 5 matches without number
        2. 12 matches with number

        :param address1:
        :param address2:
        :return:
        """
        address1 = set(address1.lower().replace(',', '').replace('  ', ' ').split(' '))
        address2 = set(address2.lower().replace(',', '').replace('  ', ' ').split(' '))

        max_str_length, diff_str_len = 0, 0
        if len(address1) > len(address2):
            max_str_length, diff_str_len = len(address1), len(address1 - address2)
        else:
            max_str_length, diff_str_len = len(address2), len(address2 - address1)

        return 100-((diff_str_len/max_str_length)*100)

    def address_match_process(self, methodology='', accuracy=80):
        """
        this function is responsible for processing the to datasets
        :param methodology:
        :param accuracy:
        :return:
        """
        counter = 0
        flag = []
        for address1 in self.address_data1:
            for address2 in self.address_data2:
                if methodology == 'levenshtein':
                    result = self.levenshtein_address_match(address1, address2)

                    if result[1] > accuracy:
                        counter = 1
                elif methodology == 'fuzzy':
                    result = self.fuzzy_wuzzy_address_match(address1, address2)

                    if result[2] > accuracy:
                        counter = 1

                else:
                    result = self.address_match_using_set(address1, address2)

                    if result > accuracy:
                        counter = 1

            flag.append(counter)
            counter = 0
        print("counter")
        return pd.DataFrame(flag)
