### Umer Mughal
### urmughal1@gmail.com
I always enjoy modular and object-oriented ways to approach things in programming.
If you have any Question, regarding anything please don't hesitate to contact me.

About the task, I hope you will enjoy as much as I enjoyed while doing. I have completed all task 
also the Airflow one without having prior knowledge.

## Requirements
* Docker 19.03.13 or above
* Docker compose 1.25.4 or above
* Linux (Recommended)
* python version 3.9 or above

## Get started
Let's get into it, to start lets have a look at the repo what included in it

### Structure of the Repo
1. AddressMatchingAlgorithm -> TASK1-AddressMatching: consist of reading module and algorithms to matching the address
2. AirFlow -> TASK2-AirFlow: docker, airflow file and requirements.txt files can be found there
3. AsyncReportGenerator -> TASK3-AsyncReportGenerator: related material 
4. OmmaxFile -> Question needed to be answered
5. docker-compose.yml file -> main docker file to run
6. python_algorithm_dataset -> dataset for address matching
7. SqlFunctions -> TASK4-SQL: sql related files 
8. Readme.md ->
9. main.py -> Main file to run Task1 and Task3
10. testing.py -> testing for Task1 and Task3

## Tasks
there were 4 task to do. each task was based on different scenario.
possible description and solutions are as follows.

### TASK1-AddressMatching
Quite famous string matching problem. Its consist of three algorithms. each algorithm has its own pro and cons 
which have been described below.

#### levenshtein_address_match
Most commonly used function, and It's pretty straight forward.
it calculates the distance between two strings a and b and
provide the ratio and distance in output
* pros: fast solution, widely used, efficient with same number of words in two strings
* cons: not good with different order of words in two string, spelled out of order,considerable spelling variation

Results:
1. 107 matches without number
2. 81 matches with number

#### fuzzy_wuzzy_address_match
good algorithm to process the matching of two string
* pros:good with different order of words in two string, spelled out of order,considerable
* cons:not fast since it has to do sorting, comparing and calculating the accuracy

Results:
1. 953 matches without number
2. 748 matches with number

#### address_match_using_set
this is the fast algorithm which is based on set which provide the fast solution.
* pros: fast 
* cons: work with same no of words in two strings


Results:
1. 5 matches without number
2. 12 matches with number

### TASK2-AirFlow
previously with 0(zero) knowledge for creating the airflow file but with possible assumption file is created.

the process or DAG that I am using is simpler. First I will get the data then put it into the S3
afterward I will process the data which is already available local and do possible conversion, after that I will 
save it into the staging database. At the end I will grab the data from staging and put it into the
production system. The process steps are as follows 

#### fetch_api_data
Gets api data from client

conditions:
1. get data at first call (it's not supporting the pagination yet and multiple call simultaneously )
2. for multiple resources we need to iterate it
3. with possibly getting the data in base currency

#### saving_data_to_s3
boto3 library is used to save the data into aws s3. I created a function that
take the different security parameters and will save the data into s3

#### staging_server_convert_cleanup

there are a couple of things are happening here
 1. getting the locally saved client data 
 2. processing the client data to different currency
 3. making connection to staging database
 4. updating the data if exist or creating new one
 5. committing to database

 NOTE: this process can be divided into multiple modules

#### saving_data_to_production
 I am assuming here to synchronize the data
 we have got from the staging and updating or
 inserting into production


### TASK3-AsyncReportGenerator
please follow the main.py for more detail

### TASK4-SQL
please follow the SqlFunctions folder 

